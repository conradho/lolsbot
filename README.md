OVERVIEW
========
- first time users will get assigned an insult or a praise
- non-first-time users with any messages like `what am i`, `vat am i?` etc will trigger insult/praise again
- `reset` and `!!` somewhere in a message will reset everyone's insults/praises
- `mute` and `unmute` will stop lolsbot from talking
- eliza the therapist
	- `<username> needs a shrink` will get eliza the therapist to talk to him
	- `<username> has recovered` will stop

INSTRUCTIONS
============
- run `python3.4 -m unittest` to make sure you didn't break anything plz
- setup with slack:
	- configure "outgoing webhook" integration
		- note the token
		- the url is <your-website>/message
		- one way to do it is to make it listen to #general with no triggers
- setup on PythonAnywhere
	- just clone this repo
	- start a new flask app
	- set `os.environ['SLACK_TOKEN'] = <your-token>` in your wsgi file before you import the flask app


OTHER NOTES
===========
- for eliza
	- downloaded from http://www.jezuk.co.uk/cgi-bin/view/software/eliza
	- ran 2to3, switched out depreciated whrandom and string stuff


TODO
====
- use sqlalchemy + mysql
- refactor app
