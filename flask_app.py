
# A very simple Flask Hello World app for you to get started with...

from collections import defaultdict
from flask import Flask, request
import json
import os
import random

from eliza import eliza

app = Flask(__name__)

# to debug
## import sys
# print(request.args, file=sys.stderr)
# print(request.values, file=sys.stderr)

class UserInsultMatcher(object):
    """this would prob be a db connection thingie instead"""
    insults = ('You are a disgrace to the human race', 'Go away')
    praises = ('HI SEXY', '<3<3')

    def __init__(self):
        self._users_insult_dict = defaultdict(self.choose_random_praise_or_insult)
        self._eliza_conversations = []
        self.mute = False
        self._eliza = eliza()

    def choose_random_praise_or_insult(self):
        if random.random() < 0.5:
            return random.choice(self.insults)
        return random.choice(self.praises)

    def match(self, username, convo_message):
        if username == 'gypsydave5':
            return 'hi wilykat <3<3'
        if username in self._eliza_conversations:
            return self._eliza.respond(convo_message)
        return self._users_insult_dict[username]

    def have_records_for(self, username):
        return username in self._users_insult_dict

    def start_talking_to(self, username):
        self._eliza_conversations.append(username)

    def stop_talking_to(self, username):
        try:
            self._eliza_conversations.remove(username)
        except:
            pass

    def reset(self):
        self._users_insult_dict = defaultdict(self.choose_random_praise_or_insult)


def apply_lolsbot_response_logic(matcher, username, convo_message):
    username = username.lower()
    if 'reset' in convo_message and '!!' in convo_message:
        ## print('resetting', file=sys.stderr)
        matcher.reset()
        return 'what a sore loser...'

    if 'unmute' in convo_message and matcher.mute == True:
        matcher.mute = False
        return 'IMM BACK!'
    if 'mute' in convo_message and 'unmute' not in convo_message:
        matcher.mute = True
    if matcher.mute:
        return

    if 'needs a shrink' in convo_message:
        matcher.start_talking_to(convo_message.split()[0])
        return 'okay, i can do that'
    if 'has recovered' in convo_message:
        matcher.stop_talking_to(convo_message.split()[0])
        return 'that sounds doubtful...'


    if matcher.have_records_for(username):
        if not any([
            'at am i' in convo_message,
            username in matcher._eliza_conversations
        ]):
            # unless user explicity says what am i/vat am i/wat am i
            # just return without saying anything
            return
    return matcher.match(username, convo_message)


USER_INSULT_MATCHER = UserInsultMatcher()

@app.route('/message', methods=['POST'])
def respond_to_chat_message_post_request():
    # token to confirm that message came from slack
    assert request.form.get('token') in os.environ['SLACK_TOKENS'].split('||')

    # don't reply to yourself
    name = request.form['user_name']
    if not name == 'slackbot':
        message = apply_lolsbot_response_logic(
            USER_INSULT_MATCHER, name, request.form['text']
        )
        if message is not None:
            return json.dumps({'text': message})
    return '200 OK'
