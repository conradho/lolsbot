import unittest
from unittest.mock import patch

from flask_app import (
    UserInsultMatcher,
    apply_lolsbot_response_logic,
)


class UserInsultMatcherTest(unittest.TestCase):

    def test_users_defaultdict_uses_correct_factory(self):
        matcher = UserInsultMatcher()
        self.assertEqual(
            matcher._users_insult_dict.default_factory,
            matcher.choose_random_praise_or_insult
        )

    def test_choose_random_praise_or_insult(self):
        pass

    def test_stop_talking_to_unknow_username_doesnt_barf(self):
        matcher = UserInsultMatcher()
        matcher.stop_talking_to('abc')


class ApplyLolsbotResponseLogicTest(unittest.TestCase):

    def setUp(self):
        self.matcher = UserInsultMatcher()
        self.matcher.match('existing-user', 'random message')
        self.assertTrue(self.matcher.have_records_for('existing-user'))

    def test_converts_everything_to_lower_case(self):
        apply_lolsbot_response_logic(self.matcher, 'RaNdOm', 'hi')
        self.assertTrue(self.matcher.have_records_for('random'))

    def test_generates_insult_or_praise_for_new_user(self):
        self.assertFalse(self.matcher.have_records_for('new-user'))
        self.assertIsNotNone(
            apply_lolsbot_response_logic(self.matcher, 'new-user', 'hi')
        )

    def test_returns_none_for_existing_user(self):
        self.assertIsNone(
            apply_lolsbot_response_logic(self.matcher, 'existing-user', 'hi')
        )

    def test_wat_am_i_generates_for_new_user(self):
        self.assertFalse(self.matcher.have_records_for('new-user'))
        self.assertIsNotNone(
            apply_lolsbot_response_logic(self.matcher, 'new-user', 'but wat am i?')
        )

    def test_wat_am_i_returns_recorded_insult_for_existing_user(self):
        self.assertTrue(self.matcher.have_records_for('existing-user'))
        self.assertIsNotNone(
            apply_lolsbot_response_logic(self.matcher, 'existing-user', 'but wat am i?')
        )

    def test_reset_is_recognized(self):
        message = apply_lolsbot_response_logic(self.matcher, 'existing-user', 'plz reset!!!')
        self.assertEqual(message, 'what a sore loser...')
        self.assertFalse(
            self.matcher.have_records_for('existing-user')
        )

    def test_can_add_to_eliza_convo_list(self):
        message = apply_lolsbot_response_logic(self.matcher, 'existing-user', 'abc needs a shrink')
        self.assertEqual(message, 'okay, i can do that')
        self.assertIn('abc', self.matcher._eliza_conversations)


    def test_eliza_is_called_when_someone_on_convo_list_speaks(self):
        self.matcher._eliza_conversations.append('existing-user')
        with patch.object(self.matcher._eliza, 'respond', return_value='hi i am eliza') as mock_respond:
            message = apply_lolsbot_response_logic(self.matcher, 'existing-user', 'i want to get a response')
        self.assertEqual(message, 'hi i am eliza')
        mock_respond.assert_called_once_with('i want to get a response')

    def test_can_remove_from_eliza(self):
        self.matcher._eliza_conversations.append('abc')
        message = apply_lolsbot_response_logic(self.matcher, 'existing-user', 'abc has recovered')
        self.assertEqual(message, 'that sounds doubtful...')
        self.assertNotIn('abc', self.matcher._eliza_conversations)

    def test_can_mute(self):
        self.assertFalse(self.matcher.mute)
        message = apply_lolsbot_response_logic(self.matcher, 'existing-user', 'mute')
        self.assertEqual(message, None)
        self.assertTrue(self.matcher.mute)

    def test_muting_persists(self):
        apply_lolsbot_response_logic(self.matcher, 'existing-user', 'mute')
        message = apply_lolsbot_response_logic(self.matcher, 'new-user', 'hi im new here, school me')
        self.assertEqual(message, None)

    def test_can_unmute_as_existing_user(self):
        self.matcher.mute = True
        message = apply_lolsbot_response_logic(self.matcher, 'existing-user', 'unmute')
        self.assertEqual(message, 'IMM BACK!')
        self.assertFalse(self.matcher.mute)

    def test_can_unmute_as_new_user(self):
        self.matcher.mute = True
        message = apply_lolsbot_response_logic(self.matcher, 'new-user', 'unmute')
        self.assertEqual(message, 'IMM BACK!')
        self.assertFalse(self.matcher.mute)

    def test_unmute_has_no_effect_if_not_muted(self):
        message = apply_lolsbot_response_logic(self.matcher, 'new-user', 'unmute')
        self.assertNotEqual(message, 'IMM BACK!')

class RespondToChatMessageePostRequestTest(unittest.TestCase):
    def DONTtest_returns_json_or_none(self):
        pass

    def DONTtest_errors_on_wrong_token(self):
        pass

    def DONTtest_does_not_reply_to_own_message(self):
        pass

